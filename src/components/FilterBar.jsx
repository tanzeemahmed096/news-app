import React, { Component } from 'react'
import "../css/filterBar.css"

class FilterBar extends Component {
  render() {
    return (
      <div className='filterBar'>
        <label htmlFor="stories">Search</label>
        <select id='stories'>
            <option value="all">All</option>
            <option value="stories">Stories</option>
            <option value="comments">Comments</option>
        </select>

        <label htmlFor="popularity">by</label>
        <select id='popularity' onChange={(e) => this.props.handleFilter(e)}>
            <option value="score">Popularity</option>
            <option value="date">Date</option>
        </select>

        <label htmlFor="date">for</label>
        <select id='date'>
            <option value="allTime">All Time</option>
            <option value="last24hours">Last 24h</option>
            <option value="pastWeek">Past Week</option>
            <option value="pastMonth">Past Month</option>
            <option value="pastYear">Past Year</option>
        </select>
      </div>
    )
  }
}

export default FilterBar