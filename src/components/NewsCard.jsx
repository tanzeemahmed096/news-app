import React, { Component } from "react";
import "../css/newsSection.css"

class NewsCard extends Component {
  render() {
    const { title, url, score, by, newsDate, comments } = this.props.news;

    return (
      <div className="newsCard">
        <div className="titleContainer">
          <p className="title">{ title }</p>
          <a href={url}>({url})</a>
        </div>

        <div className="pointsContainer">
            <small>{ score } points</small>
            <small> | </small>
            <small> { by } </small>
            <small> | </small>
            <small> { newsDate } </small>
            <small> | </small>
            <small> { comments } comments </small>
        </div>
      </div>
    );
  }
}

export default NewsCard;
