import React, { Component } from "react";
import "../css/searchBar.css";

class SearchBar extends Component {
  render() {
    return (
      <div className="searchBar">
        <input type="text" placeholder="Search stories by title or author." onChange={(e) => this.props.handleSearch(e)}/>
      </div>
    );
  }
}

export default SearchBar;
