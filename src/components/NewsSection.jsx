import React, { Component } from 'react'
import NewsCard from './NewsCard';
import "../css/newsSection.css"

class newsSection extends Component {
  render() {
    const { dataLoaded, filteredNews } = this.props;
    return (
      <div className='newsSection'>
      {
        filteredNews.length === 0 ? <h1>{ dataLoaded ? "News Not Found..." : "Loading Data..." }</h1> : filteredNews.map(news => {
          return <NewsCard key={news.id} news={news} />
        })
      }
      </div>
    )
  }
}

export default newsSection