import React, { Component } from "react";
import "./App.css";
import SearchBar from "./components/SearchBar";
import FilterBar from "./components/FilterBar";
import NewsSection from "./components/NewsSection";

class App extends Component {
  state = {
    filteredNews: [],
    fetchedNews: [],
    newsTitle: "Title",
    filteredInput: "score",
    dataLoaded: false,
  };

  getNewsById(id) {
    return fetch(
      `https://hacker-news.firebaseio.com/v0/item/${id}.json?print=pretty`
    )
      .then((res) => res.json())
      .then((data) => {
        data.newsDate = data.time
          ? new Date(data.time * 1000).toDateString()
          : null;
        data.comments = data.kids ? data.kids.length : 0;
        data.score = data.score ? data.score : 0;
        data.date = data.time ? new Date(data.time * 1000) : new Date(0);
        return data;
      })
      .catch((err) => Promise.reject(err));
  }

  sortedNews(newsArray) {
    let sortKey = this.state.filteredInput;
    switch (sortKey) {
      case "score":
        sortKey = "score";
        break;
      case "comments":
        sortKey = "comments";
        break;
      case "date":
        sortKey = "date";
        break;
      default:
        sortKey = "score";
        break;
    }

    this.setState({
      filteredNews: newsArray.sort((a, b) => b[sortKey] - a[sortKey]),
    });
  }

  getNewsIds() {
    const noOfNewsFetched = 100;
    fetch("https://hacker-news.firebaseio.com/v0/topstories.json?print=pretty")
      .then((res) => res.json())
      .then((newsIds) => {
        const promises = [];
        for (let idx = 0; idx < noOfNewsFetched; idx++) {
          promises.push(this.getNewsById(newsIds[idx]));
        }

        return Promise.all(promises);
      })
      .then((newsList) => {
        const newsArr = [...newsList];
        const sortBasedOnPoints = newsArr.sort((a, b) => b.score - a.score);
        this.setState({
          filteredNews: sortBasedOnPoints,
          fetchedNews: sortBasedOnPoints,
          dataLoaded: true,
        });
      })
      .catch((err) => console.log(err));
  }

  componentDidMount() {
    this.getNewsIds();
  }

  handleSearchInput = (e) => {

    if(e.target.value === ""){
      this.setState({
        filteredNews: [...this.state.fetchedNews]
      });
      return;
    }

    const filterNewsOnInput = this.state.fetchedNews.filter((news) => {
      return (
        news.title.toLowerCase().includes(e.target.value.toLowerCase()) || news.by.toLowerCase().includes(e.target.value.toLowerCase())
      );
    });

    this.setState({
      filteredNews: [...filterNewsOnInput],
      newsTitle: e.target.value,
    });
  };

  handleFilterInput = (e) => {
    this.setState({
      filteredInput: e.target.value,
    }, () => {
      this.sortedNews(this.state.filteredNews);
    });
  };

  render() {
    console.log(this.state);
    return (
      <div className="App">
        <SearchBar handleSearch={this.handleSearchInput} />
        <FilterBar handleFilter={this.handleFilterInput} />
        <NewsSection
          filteredNews={this.state.filteredNews}
          dataLoaded={this.state.dataLoaded}
        />
      </div>
    );
  }
}

export default App;
